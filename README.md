### Script Dependencies

* Numpy
* Matplotlib
* csv

### Usage

Upon execution the script will display an animation of particles following the trajectory given by the Lorenz Attractor equations.

There are options to save the animation in a .mp4 format and save labeling vectors in a .csv file.

There are options for displaying the labeling vectors in case there is doubt of the accuracy of these. The whole trajectory can also be displayed (**NOTE**: this was included to have a general idea of the movement. It is not recommended you use it with too many particles since it requires a lot of memory space).