import numpy as np
import pandas as pd
import random 
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation


plt.rc('contour', negative_linestyle='solid')

def gen_randomdata(data, quantity,lower_limit,upper_limit):
    for i in range(quantity):
        new = random.uniform(lower_limit,upper_limit)
        data[i]=new
    return data

#Function for calculating the derivatives
def lorenz(x, y, z, s=10, r=28, b=2.667):
    '''
    Given:
       x, y, z: a point of interest in three dimensional space
       s, r, b: parameters defining the lorenz attractor
    Returns:
       x_dot, y_dot, z_dot: values of the lorenz attractor's partial
           derivatives at the point x, y, z
    '''
    x_dot = s*(y - x)
    y_dot = r*x - y - x*z
    z_dot = x*y - b*z
    return x_dot, y_dot, z_dot


def displace(num):
    for i in range(num_particles):
        x_dot, y_dot, z_dot = lorenz(xs[num,i], ys[num,i], zs[num,i],sig[i],rho[i],beta[i])
        #Obtaining the new position
        xs[num+1,i]= xs[num,i] + (x_dot * dt)
        ys[num+1,i]= ys[num,i] + (y_dot * dt)
        zs[num+1,i] = zs[num,i] + (z_dot * dt)
    return xs, ys, zs

def update_graph(step):
      #updates the position of all the scatter points 
      scatters._offsets3d = (xs[step,:], ys[step,:], zs[step,:])
      #updates the positicion of the vectors
      if (show_trajectory == False) and show_quiver and(step < num_steps):
          global quiver
          quiver.remove()
          quiver = ax.quiver(xs[step,:], ys[step,:], zs[step,:],(xs[step+1,:] - xs[step,:]), (ys[step+1,:] - ys[step,:]), (zs[step+1,:] - zs[step,:])) 
      title.set_text('Lorenz Attractor, Step = {}'.format(step))

#Program Parameters
dt = 0.01               #size of step for lorenz
num_steps = 1000        #Number of many that are taken in total
num_particles = 100       #Number of particles taken
save = True            #If true, saves the animation as mp4
show_quiver = True      #If true, the animation presents quivers indicating next position
show_trajectory = False  #If true, shows the whole trajectory

#Ranges for lorenz parameters
lim_sig = [10,20]
lim_rho = [30,40]
lim_beta = [2.5,3.0]

#Ranges for initial coordinates
lim_xs = [0,15]
lim_ys = [0,40]
lim_zs = [0,25]

#NOTE: Both the parameters and initial values are obtained randomly
#Lorenz Parameters
sig_ini = np.empty((1, num_particles))
rho_ini = np.empty((1, num_particles))
beta_ini = np.empty((1, num_particles))

sig = gen_randomdata(sig_ini[0], num_particles, lim_sig[0], lim_sig[1])
rho = gen_randomdata(rho_ini[0], num_particles, lim_rho[0], lim_rho[1])
beta = gen_randomdata(beta_ini[0], num_particles, lim_beta[0], lim_beta[1])

# Need one more for the initial values
xs = np.empty((num_steps+1, num_particles))
ys = np.empty((num_steps+1, num_particles))
zs = np.empty((num_steps+1, num_particles))

# Set initial values
xs[0,:] = gen_randomdata(xs[0,:], num_particles, lim_xs[0],lim_xs[1])
ys[0,:] = gen_randomdata(ys[0,:], num_particles, lim_ys[0],lim_ys[1])
zs[0,:] = gen_randomdata(zs[0,:], num_particles, lim_zs[0],lim_zs[1])

# Step through "time", calculating the partial derivatives at the current point
# and using them to estimate the next point
for i in range(num_steps):
    xs, ys, zs = displace(i)

# Plot
fig = plt.figure()
ax = p3.Axes3D(fig)

#Draws the particles at their initial coordinates
scatters = ax.scatter(xs[0,:], ys[0,:], zs[0,:], s=0.01)

#Draws the vectors at their initial coordinates
if (show_trajectory == False) and show_quiver:
    quiver = ax.quiver(xs[0,:], ys[0,:], zs[0,:],(xs[1,:] - xs[0,:]), (ys[1,:] - ys[0,:]), (zs[1,:] - zs[0,:]))

#Running this for loop allows to see the whole trajectory
if show_trajectory:
    for i in range(num_steps):
        ax.quiver(xs[i,:], ys[i,:], zs[i,:],(xs[i+1,:] - xs[i,:]), (ys[i+1,:] - ys[i,:]), (zs[i+1,:] - zs[+i,:]))

ax.set_xlim3d([-50.0, 60.0])
ax.set_xlabel("X Axis")

ax.set_ylim3d([-50.0, 60.0])
ax.set_ylabel("Y Axis")

ax.set_zlim3d([-50.0, 60.0])
ax.set_zlabel("Z Axis")

title = ax.set_title('Lorenz Attractor')

plot_ani = animation.FuncAnimation(fig, update_graph, frames=num_steps, interval=50, blit=False)

if save:
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=30, metadata=dict(artist='Me'), bitrate=1800, extra_args=['-vcodec', 'libx264'])
        plot_ani.save('Lorenz.mp4', writer=writer)

plt.show()
